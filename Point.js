function Point ( x,  y) {
	this.x = x;
	this.y = y;
};

Point.prototype.distance = function (point) {
	var x = this.x - point.x;
	var y = this.y - point.y;
	return Math.sqrt(x*x + y*y);
}

Point.prototype.str = function ()
{
	return "(" + this.x + "," + this.y + ")";
}

function Vector ( p,  q) {
	this.point1 = p;
	this.point2 = q;
	this.xunit = p.x - q.x;
	this.yunit = p.y - q.y;
};

Vector.prototype.norm = function ()
{
	return Math.sqrt(this.xunit*this.xunit + this.yunit*this.yunit);
}

Vector.prototype.dot = function (vec2)
{
	return this.xunit*vec2.xunit + this.yunit*vec2.yunit;
}

Vector.prototype.angle = function (vec2)
{
	return Math.acos(this.dot(vec2)/(this.norm() * vec2.norm()));
}


Vector.prototype.normalAtDistance = function (d) {
	var midPoint = new Point ((this.point1.x + this.point2.x)/2, (this.point1.y + this.point2.y)/2);
	var x1 = this.point1.x; 
	var y1 = this.point1.y;
	var x2 = this.point2.x;
	var y2 = this.point2.y;
	var x = midPoint.x  + d * (y2-y1) / (Math.sqrt((y2-y1) * (y2-y1) + (x2-x1) * (x2-x1)));
	if (y2 - y1 == 0) {
		var y = midPoint.y;
	}
	else {
		var y = midPoint.y + d * ((x1- x2)/(y2-y1)) * (y2-y1) / (Math.sqrt((y2-y1) * (y2-y1) + (x2-x1) * (x2-x1)));
	}
	return new Point(x, y);
}

Vector.prototype.slope = function () {
	return (this.point2.y - this.point1.y)/(this.point2.x - this.point1.x);
};

Vector.prototype.solve = function ( point) {
	var y2 = this.point2.y,
		y1 = this.point1.y,
		x1 = this.point1.x,
		x2 = this.point2.x,
		x = point.x,
		y = point.y;
	var k = ((y2-y1) * (x - x1) - (x2-x1)*(y-y1))/((y2-y1)*(y2-y1) + (x2-x1)*(x2-x1));
	var a = x - k * (y2 - y1);
	var b = y + k * (x2 - x1); 
	return new Point(a, b);
};

function Problem () {
	this.pivots = []; 
	this.pivotsPriority = [];
	this.point;
	this.iterations = 0;
	this.tolerance = 0.01;
	this.agent;
	this.projection;
	this.status = false;
	this.chaos = true;
	this.chaosProbability = 0.0;
	this.cutoff = 10000;
	this.useStrictPivot = false;
	this.trials = 0;
}

Problem.prototype.addChaos = function (val)
{
	this.chaos = val;
}

Problem.prototype.setChaosProbability = function (val)
{
	this.chaosProbability = val;
}

Problem.prototype.setCutOff = function (val)
{
	this.cutoff = val;
}

Problem.prototype.addPivotPoint = function(point) {
	this.pivots.push(point);
	this.pivotsPriority.push(point);
}

Problem.prototype.setAgent = function (point) {
	this.agent = point;
	this.projection = point;
}

Problem.prototype.setPoint = function (point) {
	this.point = point;
}
Problem.prototype.updateStats = function() {
	
	if (this.status == false && this.iterations < this.cutoff) {
		var v = new Vector(this.point, this.projection);
		var point1 = v.normalAtDistance(1000);
		var point2 = v.normalAtDistance(-1000);
		drawer.drawLine(point1, point2, '#8C0000');
		drawer.drawLine(this.point, this.projection, '#8C0000');
	}
	return;
}

Problem.prototype.triangleSolve = function () {
	this.trials++;
	while (this.projection.distance(this.point) > this.tolerance && this.iterations < this.cutoff)
	{
		var pivotFound = false;
		var strictPivot = null;
		var goodPivot = null;
		var ordinaryPivot = null;
		for (var i=0; i<this.pivotsPriority.length; i++) {
			pivot = this.pivotsPriority[i];
			var distance = pivot.distance(this.projection);

			if (distance > pivot.distance(this.point)) {
				pivotFound = true;
				ordinaryPivot = pivot;
				var vecPointToTest = new Vector(this.point, this.projection);
				var vecPivotToPoint = new Vector(pivot, this.point);
				var vecTestToPivot = new Vector(pivot, this.projection);

				var thetaTestPointPivot = vecPointToTest.angle(vecPivotToPoint);
				var thetaPointTestPivot = vecTestToPivot.angle(vecPointToTest);

				if (thetaTestPointPivot >= Math.PI/2 && this.useStrictPivot == true)
				{
					strictPivot = pivot;
					break;
				}
				if (thetaPointTestPivot >= Math.PI/2)
				{
					goodPivot = pivot;
				}
			}
		}
		if (pivotFound)
		{
			var bestPivot = strictPivot || goodPivot || ordinaryPivot;
			
			var vecTestToBestPivot = new Vector(this.projection, bestPivot); 
			var rand = Math.random();
				if (rand < this.chaosProbability)
					newProjection = new Point((this.projection.x + bestPivot.x)/2, (this.projection.y + bestPivot.y)/2);
				else
					newProjection = vecTestToBestPivot.solve(this.point);
			drawPoint(newProjection);
			drawer.drawLine(this.projection, newProjection);
			this.projection = newProjection;
			this.iterations += 1;
		}
		else 
			break;
	}
	if (this.projection.distance(this.point) < this.tolerance) {
		this.status= true;
	}
	this.updateStats();
	return;
}

Problem.prototype.updatePriority = function(priorityArray) {
	if (priorityArray.length != this.pivots.length)
		return;

	this.pivotsPriority = [];
	for (var i=0; i < priorityArray.length; i++) {
		this.pivotsPriority.push(this.pivots[priorityArray[i]]);
	}
}

function Drawer () {
	this.c = document.getElementById("myCanvas");
	this.ctx = this.c.getContext("2d");
	this.ctx.lineWidth=1;
}

Drawer.prototype.drawLine = function (p1, p2, style) {
	this.ctx.beginPath();
	this.ctx.moveTo(p1.x, p1.y);
	this.ctx.lineTo(p2.x, p2.y);
	this.ctx.strokeStyle = "#909090"
	if (style)
		this.ctx.strokeStyle = style;
	this.ctx.stroke();
}
Drawer.prototype.lineTo = function (p2) {
	this.ctx.strokeStyle = "#5F5E5E"
	this.ctx.lineTo(p2.x, p2.y);
	this.ctx.stroke();
}
Drawer.prototype.drawCircle = function (p, radius, fillStyle) {
	this.ctx.fillStyle = fillStyle;
	this.ctx.strokeStyle = fillStyle;
	this.ctx.beginPath();
	this.ctx.arc(p.x, p.y, radius, 0, 2*Math.PI, false);
	this.ctx.fill();
	this.ctx.lineWidth = 1;
	this.ctx.stroke();
	this.ctx.fillStyle="#000000";
	this.ctx.strokeStyle = "#000000";

}
Drawer.prototype.drawPoint = function (p, fillStyle, pointsize) {
	this.ctx.fillStyle = fillStyle;
	var size = pointsize;
	this.drawCircle(p, size, fillStyle);
}
Drawer.prototype.drawPivot = function (p) {
	this.drawPoint(p, "#000000", 2);
}
Drawer.prototype.drawAgent = function (p) {
	this.drawPoint(p, "#0B3B0B", 1);
}

Drawer.prototype.drawP = function (p) {
	this.drawPoint(p, '#8C0000', 1);
}
Drawer.prototype.moveTo = function (p) {
	this.ctx.moveTo(p.x, p.y);
}
Drawer.prototype.clear = function () {
	this.ctx.clearRect(0, 0, this.c.width, this.c.height);
	this.ctx.beginPath();
}

function PageStats () {
	this.isPrinted = false;
	this.normalizingFactor = 10;
	this.normalize = function (val)
	{
		return (val/this.normalizingFactor).toFixed(1);
	}
}
PageStats.prototype.printPoints = function (problem) {
	if (this.isPrinted) return;
	var ulNode = document.getElementById('vertex_sort');
	for (var i=0; i< problem.pivots.length; i++) {
		var liNode = document.createElement("li");
		var icon = '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'
		liNode.innerHTML = icon + "(" + this.normalize(problem.pivots[i].x) + "," + this.normalize(problem.pivots[i].y) + ")";
		liNode.id=i;
		liNode.className = "ui-state-default ui-sortable-handle";
		ulNode.appendChild(liNode);
	}
	this.isPrinted = true;
}
PageStats.prototype.addRow = function (problem) {
	var table = document.getElementById('problemStats');
	var rowCount = table.rows.length;
	var row = table.insertRow(1);//rowCount);
	if (problem.status == false)
		row.className="danger";

	var trials = row.insertCell(0)
	var trialValue = document.createTextNode(problem.trials);
	trials.appendChild(trialValue);

	var point = row.insertCell(1);
	var pointValue = document.createTextNode('(' + this.normalize(problem.point.x) + ',' + this.normalize(problem.point.y) + ')');
	point.appendChild(pointValue);

	var agent = row.insertCell(2);
	var agentValue = document.createTextNode('(' + this.normalize(problem.agent.x) + ',' + this.normalize(problem.agent.y) + ')');
	agent.appendChild(agentValue);

	var convergence = row.insertCell(3);
	var convergenceValue = document.createTextNode('(' + this.normalize(problem.projection.x) + ',' + this.normalize(problem.projection.y) + ')');
	convergence.appendChild(convergenceValue);

	var tolerance = row.insertCell(4);
	var toleranceValue = document.createTextNode(problem.tolerance);
	tolerance.appendChild(toleranceValue);

	var chaos = row.insertCell(5);
	var chaosValue = document.createTextNode(problem.chaosProbability);
	chaos.appendChild(chaosValue);

	var iterations = row.insertCell(6);
	var iterationsValue = document.createTextNode(problem.iterations);
	iterations.appendChild(iterationsValue);

	var status = row.insertCell(7);
	var statusValue = document.createTextNode(problem.status);
	status.appendChild(statusValue);
}
PageStats.prototype.clearAll = function () {
	try {
		var table = document.getElementById('problemStats');
		var rowCount = table.rows.length;
		for (var i=1; i < rowCount; i++) {
			table.deleteRow(1);
		}
		var ulNode = document.getElementById('vertex_sort');
		ulNode.innerHTML = '';
		this.isPrinted = false;
	}catch (e) {
		alert(e);
	}
}
PageStats.prototype.updateMessage = function (str) {
	document.getElementById('Message').innerHTML = "Message: " + str;
}

function drawPoint (p) {
	drawer.drawAgent(p);
}

var InputReader = function (sim) {
	this.canvas = document.getElementById("myCanvas");
	this.canvas.addEventListener("mousedown", sim.getPosition, false);
	this.observe = true;
	var scope = this;

	this.cleanup = function () {
		this.canvas.removeEventListener("mousedown", sim.getPosition, false);
	}
}

function Simulation (dr) {
	var pivots = [];
	var point, randompoint;

	var drawer = dr;
	this.type = "pivot";
	var statistics = new PageStats();
	var problem = new Problem();
	var scope = this;
	this.createSortable = function(problem) {
		$("#vertex_sort").sortable({
	  		update: function (event, ui) {
	  			var order = $(this).sortable('toArray');
	  			var orderInt = [];
	  			for (var i=0; i < order.length; i++) {
	  				orderInt.push(parseInt(order[i]));
	  			}
	  			problem.updatePriority(orderInt);
	  		}
	  	});
	}

	$(function() {
    	$( "#vertex_sort" ).sortable();
    	$( "#vertex_sort" ).disableSelection();
    	scope.createSortable(problem);
 	});


	this.run = function() {
		problem.iterations = 0;
		problem.status = false;
		problem.triangleSolve();
		statistics.addRow(problem);
		delete problem.agent;
	}
	
	this.updateTolerance = function ()
	{
		var tolerance = parseFloat(document.getElementById('tolerance').value);
		problem.tolerance = tolerance
	}
	this.updateChaosProbability = function ()
	{
		var cp = document.getElementById('chaosProbability');
		var p = parseFloat(cp.value);
		if (p >= 0 && p<=1)
			problem.setChaosProbability(p);
	}
	this.toggleStrictPivot = function ()
	{
		var sp = document.getElementById('strictpivot');
		problem.useStrictPivot = sp.checked;
	}

	this.updateCutOff = function ()
	{
		var co = document.getElementById('cutoff');
		var p = parseInt(co.value);
		if (p>=0)
			problem.setCutOff(p);
	}
	this.getPosition = function (event) {
		if (problem.pivots.length == 3 && isValidPoint(problem.point) && isValidPoint(problem.agent)) {
	    	return;
	    }
		var x = new Number();
	    var y = new Number();
	    var canvas = document.getElementById("myCanvas");;

	    /*
	    if (event.x != undefined && event.y != undefined)
	    {
	      x = event.x;
	      y = event.y;
	    }
	    else // Firefox method to get the position
	    {
	      x = event.clientX + document.body.scrollLeft +
	          document.documentElement.scrollLeft;
	      y = event.clientY + document.body.scrollTop +
	          document.documentElement.scrollTop;
	    }

	    x -= canvas.offsetLeft;
	    y -= canvas.offsetTop;
		*/
		var mouse = getMouse(event, canvas);
		x = mouse.x;
		y = mouse.y;
	    if (scope.type == "pivot" ){
	    	var p = new Point(x,y);
	    	problem.addPivotPoint(p);
	    	drawer.drawPivot(p);
	    	var len = problem.pivots.length;
	    	statistics.clearAll();
	    	statistics.printPoints(problem);

	    }
	    else if (scope.type == "test" && problem.pivots.length){
	    	statistics.printPoints(problem);
	    	scope.createSortable(problem);
	    	var point = new Point(x, y);
	    	problem.setPoint(point);
	    	drawer.drawP(point);
	    	//statistics.updateMessage('<ul><li>Click on the screen to select a bunch of points</li><li>To test a point select tag from the radio button and click on the screen.</li><li>Select agent from the button and click to add an agent.</li></ul>');
	    }
	    else if (scope.type == "agent"  && problem.pivots.length && isValidPoint(problem.point)){//!isValidPoint(problem.agent)) {
	    	var randompoint = new Point(x, y);
	    	problem.setAgent(randompoint);
	    	drawer.drawAgent(randompoint);
	    	drawer.moveTo(randompoint);

	    }
	    if (problem.pivots.length >=1 && isValidPoint(problem.point) && isValidPoint(problem.agent)) {
	    	try {
	    		scope.run();
	    	} catch (exception)
	    	{
	    		console.log(exception);
	    		alert("Total memory exhausted. Please reduce the chaos probability.");
	    	}
	    }
	}
	
	var inputReader = new InputReader(this);

	this.start = function () {
		//statistics.updateMessage('<ul><li>Click on the screen to select a bunch of points</li><li>To test a point select tag from the radio button and click on the screen.</li><li>Select agent from the button and click to add an agent.</li></ul>');
	}

	this.clear = function() {
		drawer.clear();
		//statistics.updateMessage('<ul><li>Click on the screen to select a bunch of points</li><li>To test a point select tag from the radio button and click on the screen.</li><li>Select agent from the button and click to add an agent.</li></ul>');
		inputReader.cleanup();
		statistics.clearAll();
		delete problem;
	}

	this.addNewAgent = function  () {
		//statistics.updateMessage('<ul><li>Click on the screen to select a bunch of points</li><li>To test a point select tag from the radio button and click on the screen.</li><li>Select agent from the button and click to add an agent.</li></ul>');
		delete problem.agent;
		problem.agent = undefined;
	};

}


var getMouse = function(e, canvas) {
  var element = canvas, offsetX = 0, offsetY = 0, mx, my;

  stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)      || 0;
  stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)       || 0;
  styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10)  || 0;
  styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)   || 0;
  // Some pages have fixed-position bars (like the stumbleupon bar) at the top or left of the page
  // They will mess up mouse coordinates and this fixes that
  var html = document.body.parentNode;
  htmlTop = html.offsetTop;
  htmlLeft = html.offsetLeft;
  // Compute the total offset. It's possible to cache this if you want
  if (element.offsetParent !== undefined) {
    do {
      offsetX += element.offsetLeft;
      offsetY += element.offsetTop;
    } while ((element = element.offsetParent));
  }

  // Add padding and border style widths to offset
  // Also add the <html> offsets in case there's a position:fixed bar (like the stumbleupon bar)
  // This part is not strictly necessary, it depends on your styling
  offsetX += stylePaddingLeft + styleBorderLeft + htmlLeft;
  offsetY += stylePaddingTop + styleBorderTop + htmlTop;

  mx = e.pageX - offsetX;
  my = e.pageY - offsetY;

  // We return a simple javascript object with x and y defined
  return {x: mx, y: my};
}

function isValidPoint (p) {
	return p && (p instanceof Point);
}

var drawer = new Drawer();
var simulation = new Simulation(drawer);

function start() {
	registerHandler();
	simulation.start();
}

function SetPivot ()
{
	simulation.type = "pivot";
}
function SetTest ()
{
	simulation.type = "test";
}
function SetAgent ()
{
	simulation.type = "agent";
}
function clearCanvas () {
	simulation.clear();
	delete simulation;
	var ex1 = document.getElementById('pivot');
	ex1.checked = true;
	simulation = new Simulation(drawer);
	document.getElementById('chaosProbability').value = 0;
};

function addNewAgent () {
	simulation.addNewAgent();
}

function registerHandler ()
{
	var ex1 = document.getElementById('pivot');
    var ex2 = document.getElementById('test');
    var ex3 = document.getElementById('agent');
    var cp = document.getElementById('chaosProbability');
    var co = document.getElementById('cutoff');
    var tol = document.getElementById('tolerance');
    var sp = document.getElementById('strictpivot');

    ex1.onclick = SetPivot;
    ex2.onclick = SetTest;
    ex3.onclick = SetAgent;
    cp.oninput = simulation.updateChaosProbability;
    co.oninput = simulation.updateCutOff;
    tol.oninput = simulation.updateTolerance;
    sp.onchange = simulation.toggleStrictPivot;
}